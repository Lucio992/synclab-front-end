import {Component, OnInit} from '@angular/core';
import {TopologicalHttpService} from '../shared/http/topological-http.service';
import {LastValService} from '../shared/last-val.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  aot: any;
  circuits: any;
  devices: any;


  constructor(private _topologicalsHttp: TopologicalHttpService,public _lastValService: LastValService) {
  }

  ngOnInit(): void {
    this.getAot();
    this.getCircuits();
    this.getDevices();
  }

  getAot() {
    this._topologicalsHttp.getAot().subscribe((res: any) => {
      this.aot = res.aot;
    });
  }

  getCircuits() {
    this._topologicalsHttp.getCircuits().subscribe((res: any) => {
      this.circuits = res.circuits;
    });
  }

  getDevices() {
    this._topologicalsHttp.getDevices().subscribe((res: any) => {
      this.devices = res.devices;
    });
  }

  alertData(event){
    alert(JSON.stringify(event));
  }


}
