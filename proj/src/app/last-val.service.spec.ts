import { TestBed } from '@angular/core/testing';

import { LastValService } from './shared/last-val.service';

describe('LastValService', () => {
  let service: LastValService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LastValService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
