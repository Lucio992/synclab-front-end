import { Component, OnInit } from '@angular/core';
import {LastValService} from '../shared/last-val.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(public _lastValService: LastValService) { }

  ngOnInit(): void {
  }

}
