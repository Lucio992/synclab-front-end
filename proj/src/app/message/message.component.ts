import { Component, OnInit } from '@angular/core';
import {LastValService} from '../shared/last-val.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  constructor(public _lastValService: LastValService) { }

  ngOnInit(): void {
  }
}
