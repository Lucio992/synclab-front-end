import { Injectable } from '@angular/core';
import {TopologicalHttpService} from './topological-http.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LastValHttpService {

  constructor(private _httpService: HttpClient) {

  }

  getAotCode(){
    return  this._httpService.get('../../assets/json/lastVal.json');
}


}
