import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TopologicalHttpService {

  constructor(private _http: HttpClient) {
  }

  getAot() {
    return this._http.get('../../assets/json/aot.json');
  }

  getCircuits() {
    return this._http.get('../../assets/json/circuits.json');
  }

  getDevices() {
    return this._http.get('../../assets/json/devices.json');
  }

}
