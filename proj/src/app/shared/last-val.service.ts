import {Injectable, Input} from '@angular/core';
import {LastValHttpService} from './http/lastValHttp.service';
import {TopologicalHttpService} from './http/topological-http.service';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LastValService {
  lastVal: [];
  filteredResult = [];
  filteredLastVal: any;
  aotFromCircuits: any;
  filteredDevices: any;
  filteredIPDevices = [];
  checkPresent: number;
  devices = [];
  constructor(public _lastValHttpService: LastValHttpService, public _topologicalHttpService: TopologicalHttpService) {
  }

  getFilteredAot(aotCodeVal) {
    this.checkPresent = 0;

 const aotCode = aotCodeVal.aotCode;
 this.filteredDevices = [];
 this.filteredResult = [];
 this.aotFromCircuits = [];
 this._topologicalHttpService.getCircuits().subscribe((res: any) => {
      this.aotFromCircuits = res.circuits.filter((value: any) => value.aotCode === aotCode);
      this.filteredResult.push(aotCodeVal.aotCode , aotCodeVal.aotDesc);

    });
  }
  getFilteredDevices(circuitsId) {
    this._topologicalHttpService.getDevices().subscribe((res: any) => {
      this.filteredDevices = res.devices.filter((value: any) => value.circuitIdSAP === circuitsId);
      if (this.filteredResult.includes(circuitsId)){
        console.log('Already present in the array CircuitId');
      }else {
        this.filteredResult.push(circuitsId);
      }

    });
  }
  filterVal(code) {
    this._lastValHttpService.getAotCode().subscribe((res: any) => {
      this.lastVal = res.lastVal;
      this.filteredLastVal = this.lastVal.filter((value: any) => value.aotCode === code);
    });
  }
  getCity(city){
    if (this.filteredResult.includes(city)){
      console.log('Already present in the array');
    }else {
      this.filteredResult.push(city.aotDesc);

    }
  }
  getIpAddress(device){
    this._topologicalHttpService.getDevices().subscribe((res: any) => {
      this.filteredIPDevices = res.devices.filter((value: any) => value.ipAddress === device.ipAddress);
      if (this.filteredResult.includes(device.ipAddress)){
        console.log('already present');
      }else{
        console.log('success');
        this.filteredResult.push(device.ipAddress);
      }
    });

  }
  resetAll(){
    this.lastVal = [];
    this.filteredResult = [];
    this.filteredLastVal = [];
    this.aotFromCircuits = [];
    this.filteredDevices = [];
  }

  compareCircuitID(fromDevice){
    this.checkPresent = 0;

console.log(fromDevice.circuitIdSAP);
this._topologicalHttpService.getCircuits().subscribe((res: any) => {
  this.devices = res.circuits;

  if (this.devices.includes(fromDevice.circuitIdSAP)){
          this.checkPresent = 1 ;
          console.log(this.checkPresent);
        }else{
          this.checkPresent = 2 ;
          console.log(this.checkPresent);
        }
    });

  }
}
