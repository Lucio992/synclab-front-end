import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LastValService} from '../shared/last-val.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
@Input() data;
@Input() name;
@Output() outputData = new EventEmitter<any>();

  // tslint:disable-next-line:variable-name
  constructor( public _lastValService: LastValService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  getData(outputData){
    this.outputData.emit(outputData);
  }



}
