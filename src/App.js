import React from 'react';
import data from './data';
import Product from './components/sharedComp/product/Product';
import NavBar from "./components/sharedComp/header/NavBar";


function App() {
    return (
        <div className="grid-containerP">
            <header className="rowP">
                <NavBar/>
            </header>

            <main>
                <div className="rowP center">
                    {
                        data.categories.map(category => (
                            <Product key={category._id} category={category}></Product>
                        ))
                    }
                </div>

            </main>

            <footer className="rowP center">
                Tutti i diritti riservati
            </footer>
        </div>
    );
}

export default App;
