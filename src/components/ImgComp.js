import React from "react";

function ImgComp({src}) {
    /*let imgStyles={
        width:100+"%",
        height:"auto"
    }*/
    return <img className="medium" src={src} alt="slide-img" />
}
export default ImgComp;