import React from "react";



export default function NavBar() {
    return (
        <React.Fragment>


                <div>
                    <a className="brand" href="/">ecommerce</a>
                </div>
                <div className="formP">
                        <form className="form-inline my-2 my-lg-0 ">

                            <input className="form-control mr-sm-2 inputP" type="search" placeholder="Search"
                                   aria-label="Search"/>
                                <button className="buttonP" type="submit"><i
                                    className="fas fa-search"></i></button>
                        </form>
                </div>


                <div>
                    <a className="buttonNav" href="/Login">Accedi</a>
                    <a className="buttonNav" href="/signin">Registrati</a>
                </div>
        </React.Fragment>
    );
}