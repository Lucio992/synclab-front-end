import React from 'react';
import Rating from './Rating';
import Slider from "../slider/Slider";

export default function Product(props){
    const {category}=props;
    return(
        <div key={category._id} className="cardP">

                  <Slider key={category._id} category={category} ></Slider>
                
              </div>
    )
}