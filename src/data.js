const data = {
    categories: [
        {
            _id: '1',
            category: "Abbigliamento",
            images: ['/pics/abbigliamento/p1.jpg','/pics/abbigliamento/p2.jpg','/pics/abbigliamento/p3.jpg',]
        },
        {
            _id: '2',
            category: "Informatica",
            images: ['/pics/informatica/p1.jpg','/pics/informatica/p2.jpg','/pics/informatica/p3.jpg',]
        },
        {
            _id: '3',
            category: "Arredamento",
            images: ['/pics/arredamento/p1.jpg','/pics/arredamento/p2.jpg','/pics/arredamento/p3.jpg',]
        },
        {
            _id: '4',
            category: "Accessori",
            images: ['/pics/accessori/p1.jpg','/pics/accessori/p2.jpg','/pics/accessori/p3.jpg',]
        },
        {
            _id: '5',
            category: "Giochi",
            images: ['/pics/giochi/p1.jpg','/pics/giochi/p2.jpg','/pics/giochi/p3.jpg',]
        },
        {
            _id: '6',
            category: "Elettrodomestici",
            images: ['/pics/elettrodomestici/p1.jpg','/pics/elettrodomestici/p2.jpg','/pics/elettrodomestici/p3.jpg',]
        },
        {
            _id: '7',
            category: "Sport",
            images: ['/pics/sport/p1.jpg','/pics/sport/p2.jpg','/pics/sport/p3.jpg',]
        },
        {
            _id: '8',
        
            category: "Ufficio",
            images: ['/pics/ufficio/p1.jpg','/pics/ufficio/p2.jpg','/pics/ufficio/p3.jpg',]
        }
    ]
}

export default data